# All unpinned blobs below are extracted from miui_LMIGlobal_V14.0.1.0.SJKMIXM

# MiuiCamera-files
-system/priv-app/MiuiCamera/MiuiCamera.apk;OVERRIDES=Snap,Camera2,GoogleCameraGo,Aperture
system/priv-app/MiuiCamera/lib/arm64/libcamera_algoup_jni.xiaomi.so
system/priv-app/MiuiCamera/lib/arm64/libcamera_mianode_jni.xiaomi.so
system/priv-app/MiuiCamera/lib/arm/libarc_layer_sgl.so
system/priv-app/MiuiCamera/lib/arm/libcamera_arcsoft_handgesture.so
system/priv-app/MiuiCamera/lib/arm/libCameraEffectJNI.so
system/priv-app/MiuiCamera/lib/arm/libcamera_handgesture_mpbase.so
system/priv-app/MiuiCamera/lib/arm/libcamera_sound_effect.so
system/priv-app/MiuiCamera/lib/arm/libc++_shared.so
system/priv-app/MiuiCamera/lib/arm/libhandengine.arcsoft.so
system/priv-app/MiuiCamera/lib/arm/libmisoundcamerasupport.so
system/priv-app/MiuiCamera/lib/arm/libmiuiblursdk.so
system/priv-app/MiuiCamera/lib/arm/libmulti-wakeup-engine.so
system/priv-app/MiuiCamera/lib/arm64/libarc_layer_sgl.so
system/priv-app/MiuiCamera/lib/arm64/libarcsoft_deflicker.so
system/priv-app/MiuiCamera/lib/arm64/libcamera_arcsoft_handgesture.so
system/priv-app/MiuiCamera/lib/arm64/libCameraEffectJNI.so
system/priv-app/MiuiCamera/lib/arm64/libcamera_handgesture_mpbase.so
system/priv-app/MiuiCamera/lib/arm64/libcamera_mpbase.so
system/priv-app/MiuiCamera/lib/arm64/libcamera_sound_effect.so
system/priv-app/MiuiCamera/lib/arm64/libcom.xiaomi.camera.requestutil.so
system/priv-app/MiuiCamera/lib/arm64/libc++_shared.so
system/priv-app/MiuiCamera/lib/arm64/libc++.so
system/priv-app/MiuiCamera/lib/arm64/libdeflicker_jni.so
system/priv-app/MiuiCamera/lib/arm64/libdmabufheap.so
system/priv-app/MiuiCamera/lib/arm64/libhandengine.arcsoft.so
system/priv-app/MiuiCamera/lib/arm64/libijkffmpeg.so
system/priv-app/MiuiCamera/lib/arm64/libijkplayer.so
system/priv-app/MiuiCamera/lib/arm64/libijksdl.so
system/priv-app/MiuiCamera/lib/arm64/libion.so
system/priv-app/MiuiCamera/lib/arm64/libmialgo_saliency_jni.so
system/priv-app/MiuiCamera/lib/arm64/libmialgo_saliency.so
system/priv-app/MiuiCamera/lib/arm64/libmisoundcamerasupport.so
system/priv-app/MiuiCamera/lib/arm64/libmiuiblursdk.so
system/priv-app/MiuiCamera/lib/arm64/libmulti-wakeup-engine.so
system/priv-app/MiuiCamera/lib/arm64/libvideo_extra_color_converter.so
system/priv-app/MiuiCamera/lib/arm64/libvideo_extra_interpolator.so
system/priv-app/MiuiCamera/lib/arm64/libYuvWatermark.so

# Vendor-files
vendor/lib/vendor.qti.hardware.camera.device@1.0.so
vendor/lib/vendor.qti.hardware.camera.device@2.0.so
vendor/lib/vendor.qti.hardware.camera.device@3.5.so
vendor/lib64/camera.device@1.0-impl.so
vendor/lib64/camera.device@3.2-impl.so
vendor/lib64/camera.device@3.3-impl.so
vendor/lib64/camera.device@3.4-external-impl.so
vendor/lib64/camera.device@3.4-impl.so
vendor/lib64/camera.device@3.5-external-impl.so
vendor/lib64/camera.device@3.5-impl.so
vendor/lib64/camera.device@3.6-external-impl.so
vendor/lib64/vendor.qti.hardware.camera.device@1.0.so
vendor/lib64/vendor.qti.hardware.camera.device@2.0.so
vendor/lib64/vendor.qti.hardware.camera.device@3.5.so
